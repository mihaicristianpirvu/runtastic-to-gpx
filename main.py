import json
import sys
import os
import numpy as np
from datetime import datetime

from input_output import getRunsNames, readAndExtractArchive, writeArrayToGpx
from convert_algorithm import convertOne

def convertFiles(baseDir, allRunFiles, outputPath):
	os.mkdir(outputPath) if not os.path.isdir(outputPath) else []
	for item in allRunFiles:
		res = convertOne(baseDir, item)
		if not res:
			continue
		gpsEleArray, startTime = res
		writeArrayToGpx(gpsEleArray, startTime, outputPath)

def main():
	os.environ["TZ"] = "UTC"
	# convertFiles("export-20190915-000/Sport-sessions", ["55dabab16cc1cecec9380a71.json"], "export")
	# convertFiles("export-20191021-000/Sport-sessions", ["1105a5ac-8c51-4309-a202-596ddfbf0112.json"], "export")

	baseDir = readAndExtractArchive(sys.argv[1])
	files = getRunsNames(baseDir)
	convertFiles(baseDir, files, "export")

if __name__ == "__main__":
	main()
