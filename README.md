# Runtastic to GPX

This project aims to convert Runtastic data, exported from the Runtastic website (Profile -> Export -> .zip) to a list of GPX files, which can be uploaded to other sports activies websites/apps, such as Strava.

To run this tool, you must:
- Install Python3.6+ (and some other dependencies, such as numpy, via pip/conda)
- Open a terminal/CMD line
- Download/Clone this repository
```
git clone https://gitlab.com/mihaicristianpirvu/runtastic-to-gpx
```
- Run the tool, using the downloaded zip archive from Runtastic
```
[user@runtastic-to-gpx] python main.py /path/to/runtastic_data.zip
```
- Upon this call, a directory called `export` shall be created, with a list of activies in the form of `date_of_activty.gpx`
- Upload these files manually to your desired sports tracking app


## Known issues
- Strava has some algorithm to detect moving time, which may differ from real activity time. Both of them appear, however, moving time is the one that's used for statistics. I have tried to update the GPS data as good as possible to minimize the difference, by adding fake data in the GPS holes, which may appear as stops, however it's not yet perfect, but the difference is smaller than using raw data from Runtastic.
- Windows may or may not work because of how timezones are handled. Might update it later

## TODO
- GUI to chose only selected runs
- Web-app where you upload the zip archive and returns a zip archive of GPX
