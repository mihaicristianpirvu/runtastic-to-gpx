from datetime import datetime

def getTimeStampOffset(strTimeStamp):
	wherePlus = strTimeStamp.find("+")
	whereMinus = strTimeStamp.find("-")
	if wherePlus > 0:
		offset = strTimeStamp[wherePlus + 1: ]
		offset = int(offset[-2 :]) * 60 + int(offset[0 : 2]) * 3600
		strTimeStamp = strTimeStamp[0 : wherePlus - 1]
	elif whereMinus > 0:
		offset = strTimeStamp[whereMinus + 1: ]
		offset = -(int(offset[-2 :]) * 60 + int(offset[0 : 2]) * 3600)
		strTimeStamp = strTimeStamp[0 : whereMinus - 1]
	else:
		offset = 0
	return strTimeStamp, offset

# Converts from string to epoch time
def convertTimestampToEpoch(strTimeStamp):
	# Get offset and truncated string
	strTimeStamp, offset = getTimeStampOffset(strTimeStamp)

	# Convert to epoch and subtract time zone offset (Strava seems to infer this from GPS position)
	timeStamp = int(datetime.strptime(strTimeStamp, "%Y-%m-%d %H:%M:%S").timestamp()) - offset
	return timeStamp

def formatTimestampForOutput(timeStamp):
	item = str(datetime.fromtimestamp(timeStamp))
	item = "%sT%s.000Z" % (item[0 : 10], item[11 : ])
	return item
